import FreeCAD
import FreeCADGui
import Part
import os

LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
DISPLAY = True

class TestFiles:
    """Convenience class to grab test file paths"""
    simple_1 = os.path.join(LOCAL_PATH, 'parts', 'simple_1.step')

    simple_2 = os.path.join(LOCAL_PATH, 'parts', 'simple_2.step')

    simple_3 = os.path.join(LOCAL_PATH, 'parts', 'simple_3.step')

    intermediate_1 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_1.step')

    intermediate_2 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_2.step')

    intermediate_3 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_3.step')

    challenge_1 = os.path.join(LOCAL_PATH, 'parts', 'challenge_1.step')


class Types:
    THROUGH = 'through'
    BLIND = 'blind'


class Compositions:
    BASIC = 'basic'
    TAPERED = 'tapered'
    COUNTERSINK = 'countersink'
    COUNTERBORE = 'counterbore'
    COMPOUND = 'compound'
    
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class GeometryError(Error):
    def __init__(self, message):
        self.message = message

def display(obj, name, color=(0.5, 0.5, 0.5), transparency=0, visibility=True):
    """
    Utility function to display a face or group of faces
    
    :param obj: Either a single ``Part.Face``, a list or set of ``Part.Face`` 
    objects, or a ``Part.Shape`` object
    :param name: ``string`` Name to give the face or group of faces
    :param color: Three-tuple of RGB color values for the face(s) display 
    color. Must be floats so make sure the numbers in the tuple have a decimal 
    point at the end as seen in the inputs. Default color is gray
    :param transparency: Number between 0 and 1, determines how see through 
    the part is, default to 0
    :param visibility: ``Boolean`` sets default visibility of object, can be 
    toggled with space bar in FreeCAD
    """
    # check to see if a document has been created, if not, make one
    if FreeCADGui.ActiveDocument is None:
        FreeCAD.newDocument()

    if isinstance(obj, Part.Face):
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = obj
    elif isinstance(obj, list) or isinstance(obj, set):
        sh = Part.makeShell(list(obj))
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = sh
    elif isinstance(obj, Part.Shape):
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = obj
    else:
        print('Incompatible display type!')
        return

    # Now set the color and transparency.
    # After creating the object, it is always set to the Gui active object;
    # object can also be referenced with the syntax:
    # FreeCADGui.ActiveDocument.getObject(name)
    FreeCADGui.ActiveDocument.ActiveObject.DiffuseColor = color
    FreeCADGui.ActiveDocument.ActiveObject.Transparency = transparency
    FreeCADGui.ActiveDocument.ActiveObject.Visibility = visibility

def vertex_to_tuple(vertex):
    """
    param: FreeCad Vertex object
    returns: tuple containing x, y, and z coordinates
    """
    return (vertex.X,vertex.Y,vertex.Z)

def vector_to_tuple(vector):
    """
    param: FreeCad 3D Vector object
    returns: tuple containing x, y, and z componenents
    """
    return (vector.x,vector.y,vector.z)

def vector2_to_tuple(vector):
    """
    param: FreeCad 2D Vector object
    returns: tuple containing x and y componenents
    """
    return (vector.x,vector.y)

def tuples_equal(t1,t2,tolerance):
    """
    params: tuple, tuple, float
    returns: boolean
    
    """
    return abs(t1[0]-t2[0])<tolerance and abs(t1[1]-t2[1])<tolerance
def tuple_in_list(t,l,tolerance):
    """
    params: tuple, list of dicts, float
    returns: boolean, int
    Takes a tuple and list of dictionaries of hole features
    Returns if tuple is contained as a value from the center key and at what index in the list
    """
    in_list = False
    for i,d in enumerate(l):
        if(tuples_equal(t,d['center'],tolerance)):
            in_list = True
            break
    return in_list,i

def get_normal_dir(e):
    """
    params: FreeCad Edge object
    returns: int
    Take an edge and returns the normal direction of the edge as an int from 0-2
    """
    normal_tuple = vector_to_tuple(e.normalAt(0))
    largest_comp = 0
    normal_dir = 0
    for i,val in enumerate(normal_tuple):
        if abs(val >= largest_comp):
            normal_dir = i
    return normal_dir
            
def get_edge_specs(face):
    """
    params: FreeCad Face object
    returns: list of dicts
    Take a face and returns a list of dicts with each dict containing the center, radius, and normal direction of each edge the hole
    """
    edge_specs = []
    edges = face.OuterWire.OrderedEdges
    for edge in edges:
        if(edge.curvatureAt(0) != 0):
            edge_specs.append({'center':edge.centerOfCurvatureAt(0),'radius':1/edge.curvatureAt(0),'axis':get_normal_dir(edge)})
    return edge_specs

def interrogate(path_to_file):
    """
    Implementation of hole detection logic. Put your solution here! You can
    remove the example logic below.
    
    :param path_to_file: ``string`` path to the part file, use ``TestFiles`` 
    for convenience
    :return: List of dictionaries of hole features and compound hole features
    """
    # code to read the part into shape
    shape = Part.Shape()
    shape.read(path_to_file)

    face_list = list(shape.Faces)
    # data structures to hold information about various geometric components
    hole_features = []
    compound_hole_features = []
    face_dict = {}
    axes = []
    #tolerance to be used when comparing geometries
    tolerance = .00001
    
    for face in face_list:
        # check to see if face has curvature
        # location of curvature doesn't matter as if curved will be non-zero everywhere on face
        if(face.curvatureAt(0,0) != (0,0)):
            # collect vertexes of faces and put into data structure
            # two faces with common vertices will be considered a hole
            vertices_list = face.OuterWire.OrderedVertexes
            # use frozen set so order of vertices will not matter and is immutable so can be used a dictionary key
            vertices = frozenset([vertex_to_tuple(vert) for vert in vertices_list])
            if(vertices in face_dict.keys()):
                face_dict[vertices].append(face)
            else:
                face_dict[vertices] = [face]
    
    # try loop to check for IndexError which is expected if part includes hole with tapered bottom
    try:
        # loop through all faces
        for faces_key in face_dict.keys():
            faces = face_dict[faces_key]    
            # faces known to be part of a hole if two faces have same vertices
            if(len(faces) == 2):
                edge_specs = get_edge_specs(faces[0])
                # get depth from the length of difference between the two centers of the hole
                depth = edge_specs[0]['center'].sub(edge_specs[1]['center']).Length
                # get diameter from 2x the larger radius between the two edges
                diameter = 2*max(edge_specs[0]['radius'],edge_specs[1]['radius'])
                # compare the two radii to find the hole composition
                if(abs(edge_specs[0]['radius'] - edge_specs[1]['radius']) > tolerance):
                    composition = Compositions.TAPERED
                else:
                    composition = Compositions.BASIC
                
                hole_feature = {
                    'faces':faces,
                    'diameter':diameter,
                    'depth':depth,
                    'composition':composition
                    }
                hole_features.append(hole_feature)
                
                # code to complete hole_features list is complete
                # now populate compound_hole_features list
                
                # delete the component of the center in the normal direction to the planar center coordinates as a tuple
                # necessary that center coordinates be 2D for later on
                normal_dir = edge_specs[0]['axis']
                center_list = list(vector_to_tuple(edge_specs[0]['center']))
                del center_list[normal_dir]
                center = tuple(center_list)
                
                # if axes is empty, append dictionary of center and hole features,
                # if not, check center coordinates against current axes within tolerance
                # if axes found, append hole_feature to that axis
                # if not, append center coordinates as axis
                if(len(axes) > 0):
                    in_list, center_index = tuple_in_list(center,axes,tolerance)
                    if(in_list):
                        axes[center_index]['hole_feature'].append(hole_feature)
                    else:
                        axes.append({'center':center, 'hole_feature':[hole_feature]})
                else:
                    axes.append({'center':center, 'hole_feature':[hole_feature]})
        
        # now go through axes and determine if hole is compound, countersunk, counterbored, or nothing
        for axis in axes:
            axis_features = axis['hole_feature']
            # counterbored if 2 features and both basic
            if(len(axis_features)==2
                and axis_features[0]['composition']==Compositions.BASIC
                and axis_features[1]['composition']==Compositions.BASIC
            ):
                compound_hole_feature = {
                    'sub_features' : axis_features,
                    'composition' : Compositions.COUNTERBORE,
                    'depth' : axis_features[0]['depth'] + axis_features[1]['depth'],
                    'diameter' : min(axis_features[0]['diameter'],axis_features[1]['diameter'])
                }
                compound_hole_features.append(compound_hole_feature)
            # countersunk if 2 features and wider hole tapered, smaller hole basic
            elif(len(axis_features)==2
                and (
                    (
                        axis_features[0]['composition']==Compositions.TAPERED 
                        and axis_features[1]['composition']==Compositions.BASIC 
                        and axis_features[0]['diameter']>axis_features[1]['diameter']
                    ) or (
                        axis_features[1]['composition']==Compositions.TAPERED 
                        and axis_features[0]['composition']==Compositions.BASIC 
                        and axis_features[1]['diameter']>axis_features[1]['diameter']
                    )
                )
            ):
                compound_hole_feature = {
                    'sub_features' : axis_features,
                    'composition' : Compositions.COUNTERSINK,
                    'depth' : axis_features[0]['depth'] + axis_features[1]['depth'],
                    'diameter' : min(axis_features[0]['diameter'],axis_features[1]['diameter'])
                }
                compound_hole_features.append(compound_hole_feature)
            # compound if two or more features and not counterbored, countersunk
            elif(len(axis_features)>=2):
                compound_hole_feature = {
                    'sub_features' : axis_features,
                    'composition' : Compositions.COMPOUND,
                    'depth' : sum([sf['depth'] for sf in axis_features]),
                    'diameter' : min(axis_features[0]['diameter'],axis_features[1]['diameter'])
                }
                compound_hole_features.append(compound_hole_feature)
    # catch IndexError caused by hole with tapered bottom
    except IndexError:
        # raise Geometry error exception
        raise GeometryError('This shape contains a hole with a tapered bottom. This solution cannot handle this yet')
    return hole_features, compound_hole_features