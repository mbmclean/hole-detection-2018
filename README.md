# Paperless Parts Coding Challenge #

------------------------------------
Solution Explanation:

Overview of my solution:

The path I decided to take to identify holes was to look at faces that shared all
four vertices with other faces. This allowed me to quickly identify the faces that
made up each hole and ignore other curved faces that were not part of holes.
From there I identified features about the edge that made up each hole such as
the radius, center, and normal direction that gave me enough information to get 
the hole diameter, depth, and composition. This got me my list of hole_features.

From there, I grouped hole features by axis. I then iterated through each hole axis
found in the part and analyzed what the collection of hole features associated with
it indicated about the compound hole composition.

Limitations to my solution:

- I ran out of time to implement the through hole vs blind hole determination.
My solution initially used faces with common vertices as the determination of holes.
I planned to use faces common edges to try to determine through vs blind
after I got the other hole features implemented. This strategy turned out to be very 
difficult give the limited functions available on FreeCad objects and, in my opinion,
very poor documentation that was limited to a wiki page with no information about specific
methods for different FreeCad objects.

- Because I ran out of time to handle through vs blind, my solution also has trouble with
tapered bottom features. If I had more time, this would have consisted generalizing the process
of finding and pulling information from the edges of the hole to find edges or vertices of the
holes. Alternatively, this also would have been much easier if I had implemented through vs
blind, but as I mentioned I ran out of time for this.

- Because solution is not able to hand through vs blind, it throws an index out of bounds
exception when it attempts to reference the two circles that make up the edges of each hole.
To accomodate this, I created an GeometryError exception to make the handling of this a little cleaner.

- Another thing I ran out of time to implement was handling of holes who axes do not align with 
one of the 3 cartesian axes. This appears in intermediate_2 and is why my solution has issues with it.
This is because I identify axes for compound holes via the coordinates of the axis, which does not work
if the axes are not parallel to one of the cartesian axes.

- Finally, because of blind vs through not being implemented, the solution is not
able to handle multiple compound holes that share the same axes but have material between them.

What I would do differently:

Firstly, I would do a better job of creating types to represent faces, edges, vertices, axes, etc.
This would have made it much easier to store information about FreeCad objects in a
flexible accessible manner. As a result of not doing this at the start, I ended up with
lots of different data structures that appear similar but store data in slightly
different ways (ugly!). Secondly, I would from the beginning make the way that 
I identify holes more robust to better account for tapered bottoms and integrate better
with my through vs blind implementation.

Other notes:

The testing script asserts that the simple_2 part has only 1 basic hole feature.
To me, it appears that it has 2 as it has a counterbored hole consisting of 2
basic hole features. I am not sure if that is a mistake in the testing code or I
interpreted the instructions incorrectly. Either way, I changed that assertion to
2 two reflect that my code is doing what it is intended to do.

Additionally, my final commit of the test_solutions scripty contains the testing assertions that check through vs
blind commentted out In order to show the correctness of the other portions of my 
solution. Obviously, my solution would fail the blind vs through check. The commit
of the testing script also contains the above mentioned change for the simple_2
testing code.

I have added inline comments to my solution to provide a more detailed walkthrough
of specific steps of my solution.

I spent roughly 10 hours on this also some of that time was spent getting FreeCAD
installed, setting up my dev environment, etc.

------------------------------------

This exercise will introduce you to basic computational geometry with FreeCAD. 
If we've asked you to complete this challenge as part of the employment 
application process, Paperless Parts will consider your solution as a component 
of our evaluation of your technical skills and problem solving capabilities.

As with most interesting technical problems, a robust solution can take a long 
time to implement. We are not looking for you to spend too much time on this. 
Decide how much time you can dedicate, timebox your efforts, and see how far you can get. 
Communicate the number of hours worked and what you would do if you had more time.

In addition to your ability to analyze 3D geometries, here are are some of 
questions we're looking to answer: Are you able to use Git and BitBucket? Did 
you get FreeCAD installed? Could you run a Python script through the FreeCAD 
console? Did you document solution and submit in an effective way?


## Project Goals ##

For this project, you will use an open source CAD tool known as FreeCAD to 
compute certain geometric quantities from three dimensional part files. 
FreeCAD has a convenient Python scripting console that allows us to interact 
with geometries and FreeCAD features via Python commands. To solve this challenge, 
you will be writing Python code and communicating your work to us through the use 
of git.

#### Objectives ####

Your task consists of four objectives, each of increasing difficulty:

1.) Group all cones and cylinders in a 3D file into "hole features."

   A "hole feature" is defined as any collection of circular-type faces (cones and cylinders) 
   that wrap 360 degrees around a central axis and share the same start and end heights 
   relative to the central axis.

2.) For each hole feature, tell us the following:

  1. Type: `THROUGH` or `BLIND`

     A through hole is a hole drilled through the entire part. More technically, it is any 
     combination of hole features where at the smallest cross-sectional diameter you can 
     see completely through the hole the from one non-part-captive space to another 
     non-part-captive space. (Note: "non-part-captive space" means a space where there is 
     more than one direction to take to escape the bounding box of the part.)

     A blind hole is the opposite of a through hole, where there is a bottom
     to the hole feature and you cannot see "through" the part.

  2. Composition: `BASIC` or `TAPERED`

     A basic composition is the most straightforward type of hole, which consists
     of a uniform cylindrical profile from start to finish.

     A tapered composition consists of a conical profile from start to finish.

  3. Depth

  4. Diameter

     For conical sections, find the smallest diameter of the feature.

3.) Group hole features into "compound" hole features.

   A "compound" hole feature is a collection of two or more hole features that 
   share the same continuous central axis. (Note: "continuous" in this sense means that 
   the entire hole occupies the same cavity in the part file. Two separate compound hole 
   features can occupy the same central axis, but they could be separated by solid 
   material between the bottom of the two respective holes 
   (see `TestFiles.intermediate_3` for an example).

4.) For each compound hole feature, tell us the following:

  1. Type: `THROUGH` or `BLIND`

  2. Composition: `COUNTERSINK`, `COUNTERBORE`, or `COMPOUND`

     A countersink is a tapered hole on top of a basic hole.

     A counterbore is two basic holes stacked on top of each other, but with different radii.
     Note: The top hole feature in a counterbore is considered a BLIND hole, because
     you cannot see through the entire cross sectional area of the hole to another 
     non-captive space.

     A compound hole is any hole feature that does not fall under the first three categories.

  3. Depth (get the combined depth of all hole features in the compound feature)

  4. Diameter (get the smallest diameter of any hole feature in the compound feature)
  
#### Examples of Hole Features ####
  
![Image of Countersink, Counterbore Holes](https://s3.amazonaws.com/downloads.paperlessparts.com/counterbore-countersink.png)

![Hole Types](https://s3.amazonaws.com/downloads.paperlessparts.com/hole_types.gif)

A: `BLIND BASIC` and `BLIND TAPERED` compound hole feature

B: `THROUGH BASIC` hole feature

C: `BLIND TAPERED` hole feature

D: `THROUGH COUNTERBORE` compound hole feature

E: `BLIND TAPERED` and `BLIND BASIC` and `BLIND TAPERED` compound hole feature



#### Stretch Goals ####

1.) Write a function to show all the hole features or compound hole features grouped by
   color using the `display` function we have provided.

2.) Generate a report (printing to the screen is sufficient) that tells us all we need to
   know about what you have extracted from the hole features of a part in an organized
   fashion.
   
We have provided 7 STEP files to use as test cases. Three of them are marked simple, 
three of them are marked intermediate, and one is marked as a challenge. Use the simple
files to establish your basic hole detection code, and expand its robustness as you
approach the intermediate files. The challenge file is a very interesting and complicated
file, so have fun with it!


## Getting Started ##

#### Prerequisites: ####

* Install FreeCAD (latest version can be downloaded here: [https://www.freecadweb.org](https://www.freecadweb.org)).
* Create a free BitBucket account ([https://www.bitbucket.com](https://www.bitbucket.com)).
* Install Python 3.5 and familiarize yourself with lists and objects.
* Optional: Install PyCharm IDE ([https://www.jetbrains.com/pycharm/](https://www.jetbrains.com/pycharm/)).


#### Fork and clone this repository: ####

* Fork this repository to create your own private repository belonging to your BitBucket account. 
* Clone your fork (for example: `git clone https://username@bitbucket.org/username/hole-detection-2018.git`).
* Note the full path of this clone (for example, `/Users/username/path/to/hole-detection-2018`).

For more detailed instructions on how to fork a repository in BitBucket, see [Forking a Repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).

`solution.py` contains a skeleton function `interrogate()`, which is where you will put your 
solution. `test_solution.py` is the test suite we will use to help us evaluate your solution.

## Run Your Solution in FreeCAD ##

Before you can implement a solution, you will need to get familiar with loading a part and 
interacting with it in Python. First show the Python Console in FreeCAD by going to View > Panels > Python Console. From there, you can display a STEP file by running these commands in the console
(you'll need to substitute `/Users/username/path/to/hole-detection-2018/` with the location where
you cloned the repository):

    import Part
    shape = Part.Shape()
    shape.read('/Users/username/path/to/hole-detection-2018/parts/simple1.step')
    Part.show(shape)
    FreeCADGui.SendMsgToActiveView('ViewFit')

*Hint: `shape.Faces` is a list of the faces of this 3D part.*

When you're working on your solution, you should test early and often by running your
`interrogate()` function. To run your solution, first run these lines:

    import sys
    sys.path.append('/Users/username/path/to/parallel-area')
    import solution

Each time you want to run your solution, type or paste this line:

    reload(solution); solution.interrogate(solution.TestFiles.simple_1)

> Note, you can replace `solution.TestFiles.simple_1` with any of the available test files.

As you debug your solution, it may be helpful to visualize the part of a subset of its faces.
We have provided a function `solution.display()` to help you. When you use this display function,
you will notice new items populate in the application tree view. Use the spacebar to toggle
the visibility of these objects in the tree.

To run your solution against all of the available test files, type or paste these lines:

    import test_solution
    test_solution.run_tests()

When you are satisfied with your solution (or out of time!), submit it to 
Paperless Parts. (See instructions in the next section.)

## How to Submit Your Work ##

You should include a written description of your solution in any form you find effective. 
Options include inline code comments, a dedicated "readme" file, or an email.

It is a good idea to checkpoint your work regularly using git. Do this as follows:

* View the current state of your clone: `git status`
* Stage changed files for commit: `git add -u`
* If you've created new files, add them: `git add my-new-file.py`
* Commit your changes: `git commit -m "My awesome description of this commit"`
* Upload (i.e., "push") your changes to BitBucket: `git push origin master`

You may send us your solution either by email (create a ZIP or TAR archive of your git 
working directory and send to scott@paperlessparts.com) or by adding us to your private 
repository fork. To give us access to your repository, add

* Scott Sawyer (BitBucket username: 10flow)
* Dana Wensberg (BitBucket username: pint_drinker)

by following the instructions under "User access on existing repositories" found 
[here](https://confluence.atlassian.com/bitbucket/create-and-grant-access-to-team-repositories-665225545.html).

## Recommendations ##

Start by taking a look at the sample parts in the `parts` folder. This will give you an idea
of the different types of faces parts can have.

Spend some type digging into the FreeCAD syntax, particularly with the Part module. In the Python console, enter `help(Part)`, `help(Part.Face)`, and `help(Part.Edge)`. These will give you basic descriptions on all of the functions and information you have access to when referring to geometric features of the part file. Exploring the capabilities will be very useful when beginning to brainstorm.

Start simple! How can you get a list of all the cylinders in a part? All the cones?
What about these cylinders and cones make them likely to be a part of the same hole feature?
